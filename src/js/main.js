'use strict'

const burgerBtn = document.querySelector('.burger-btn');
burgerBtn.addEventListener('click', (event)=>{
    let icon;
    if(event.target.tagName==='IMG'){
        icon = event.target.closest('button');
    }else{
        icon = event.target;
    }
    icon.querySelectorAll('img').forEach(element => {
        element.classList.toggle('burger-btn__icon--none');
    });
    document.querySelector('.burger-menu__items').classList.toggle('burger-menu__items--open');
});
